let textArea = document.querySelector('#input')

let button = document.querySelector('#button');

let div = document.querySelector('#words')

function Word(value) {
    return {
        value: value,
        bind(fn) {
            return fn(this.value)
        }
    }
}

function extract_words(words) {
    return Word(words.toLowerCase().split(' '))
}

function remove_stop_words(word_list) {
    let stopWords = stop_words()
    let new_word_list = []
    new_word_list = word_list.map(word => {
        if(!stopWords.includes(word)) {
            return word
        }
    })
    new_word_list = new_word_list.filter(value => {
        if(value!==undefined){
            return value;
        }
    })
    return Word(new_word_list)
}

function frequencies(word_list) {
    let word_freqs = {}
    word_list.forEach(word => {
        if(word_freqs[word]) {
            word_freqs[word] += 1
        }else {
            word_freqs[word] = 1
        }
    })

    return Word(word_freqs)
}

function sorted(word_freqs) {
    const word_sorted = {};
    Object.keys(word_freqs).sort().forEach(key => {
        word_sorted[key] = word_freqs[key];
    });
    return Word(word_sorted)
}

function top_25(word_list) {
    const top25 = {}
    let i = 0;
    Object.keys(word_list).forEach(word => {
        if(i < 25) {
            top25[word] = word_list[word]
            i+=1
        }
    });

    return Word(top25)
}
function stop_words() {
 return ["a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren't", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't", "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's", "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves", "ante", "após", "até", "com", "de", "desde", "em", "para", "por", "sem", "sob", "como", "senão", "la", "le", "lo", "a", "o", "e", "an", "en", "so", "con", "um", "uma", "un", "una", "uno", "del", "y", "da", "já", "ya", "ser"] 
}

button.addEventListener('click',event => {
    event.preventDefault()
    let word = Word(textArea.value)
    console.log(word.bind(extract_words).bind(remove_stop_words).bind(frequencies).bind(sorted).bind(top_25).value)
});