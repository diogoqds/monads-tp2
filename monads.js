function Talvez(value) {
    if(value === null) {
        return Nada();
    } else {
        return Apenas(value);
    }
}

function Nada() {
    return {
        value: null,

        bind(fn) {
            return Nada(fn(this.value))
        }
    }
}

function Apenas(value) {
    return {
        value: value,

        bind(fn) {
            return fn(this.value)
        }
    }
}

function talvezSoma1(num) {
    return Apenas(num + 1);
}

function multiplicaCinco(num) {
    return Apenas(num * 5)
}

function somaNada(num) {
    return Nada()
}

