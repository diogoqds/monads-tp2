-- import Control.Monad

-- data Maybe a = Nothing | Just a deriving (Show)

-- instance Monad Maybe where
--   return :: a -> Maybe a
--   (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b

maybeSum1 :: Int -> Maybe Int
maybeSum1 num = Just (num + 1)

sumNothing :: Int -> Maybe Int
sumNothing _ = Nothing